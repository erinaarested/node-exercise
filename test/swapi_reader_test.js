var assert = require('assert');
var swapiReader = require('../source/swapi_reader');

describe('SwapiReader', function () {
    this.timeout(10000);
    describe('constructor (and getURL for validation)', () => {
        it('constructs with people resource as default', () => {
            reader = new swapiReader();
            assert.strictEqual(reader.getURL(), 'https://swapi.dev/api/people/')
        });

        it('constructs with custom specified resource', () => {
            reader = new swapiReader('planets/');
            assert.strictEqual(reader.getURL(), 'https://swapi.dev/api/planets/')
        });

        it('constructs with adding a trailing slash of custom specified resource missing it', () => {
            reader = new swapiReader('planets');
            assert.strictEqual(reader.getURL(), 'https://swapi.dev/api/planets/')
        });
    });

    describe('getResource', () => {
        it('gets a result from swapi', (done) => {
            reader = new swapiReader();
            reader.getResource(1, (item) => {
                assert.strictEqual('name' in item, true);
                assert.strictEqual('mass' in item, true);
                assert.strictEqual('height' in item, true);
                done();
            });
        });

        it('gets a different result for different ids', (done) => {
            reader = new swapiReader();
            reader.getResource(1, (item1) => {
                reader.getResource(2, (item2) => {
                    assert.notStrictEqual(item1, item2);
                    done();
                });
            });
        });

        it('fails if given a non-integer id', () => {
            reader = new swapiReader();
            assert.throws(() => {reader.getResource(1.5, (_) => {})}, Error);
        });

        it('fails if given an id less than 1', () => {
            reader = new swapiReader();
            assert.throws(() => {reader.getResource(0, (_) => {})}, Error);
        });

        it('gets null if given an id that does not exist', (done) => {
            // This is a more brittle test, because if more items are added in the future, new ids could exist.
            // But for now, we aren't going to worry about that by using a rediculously large id.
            reader = new swapiReader();
            reader.getPage(1000, (page) => {
                assert.strictEqual(page, null);
                done();
            });
        });
    });

    describe('getPage', () => {
        it('gets a result from swapi', (done) => {
            reader = new swapiReader();
            reader.getPage(1, (page) => {
                assert.strictEqual('count' in page, true);
                assert.strictEqual('next' in page, true);
                assert.strictEqual('previous' in page, true);
                assert.strictEqual('results' in page, true);
                assert.strictEqual(typeof(page['results']), typeof([]));
                // Note on the last assert: js array type is an object so this assertion is a little moot, but if this project is ever upgraded to Typescript, it will matter more.
                done();
            });
        });

        it('gets a different result for different pages', (done) => {
            reader = new swapiReader();
            reader.getPage(1, (page1) => {
                reader.getPage(2, (page2) => {
                    assert.notStrictEqual(page1, page2);
                    done();
                });
            });
        });

        it('fails if given a non-integer page number', () => {
            reader = new swapiReader();
            assert.throws(() => {reader.getPage(1.5, (_) => {})}, Error);
        });

        it('fails if given a page number less than 1', () => {
            reader = new swapiReader();
            assert.throws(() => {reader.getPage(0, (_) => {})}, Error);
        });

        it('gets null if given a page number larger than the highest page swapi has', (done) => {
            // This is a more brittle test, because if by some point in the future, more items are added, more pages could be added, changing the highest page number...
            // But for now, we aren't going to worry about that by using a rediculously large page number.
            reader = new swapiReader();
            reader.getPage(1000, (page) => {
                assert.strictEqual(page, null);
                done();
            });
        });
    });

    describe('getAll', () => {
        it('gets all resources from swapi', (done) => {
            reader = new swapiReader();
            reader.getAll((data) => {
                reader.getPage(1, (page) => { // Use getPage to access the count parameter and check it matches the length we got
                    assert.strictEqual(typeof(data), typeof([]));
                    assert.strictEqual(data.length, page['count']);
                    done();
                });
            });
        });
    });
});