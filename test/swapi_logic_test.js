var assert = require('assert');
var SwapiLogic = require('../source/swapi_logic');

describe('SwapiLogic', function () {
    this.timeout(10000);
    describe('constructor', () => {
        it('constructs without error', () => {
            logic = new SwapiLogic();
        });

        it('uses singleton pattern', () => {
            logic1 = new SwapiLogic();
            logic2 = new SwapiLogic();
            assert.strictEqual(logic1, logic2);
        });
    });

    describe('getPeople', () => {
        it('works without a sort', (done) => {
            logic = new SwapiLogic();
            logic.getPeople(null, (people) => {
                assert.strictEqual(typeof(people), typeof([]));
                assert.strictEqual(people.length > 0, true);
                done();
            });
        });

        it('can sort on name', (done) => {
            logic = new SwapiLogic();
            logic.getPeople('name', (people) => {
                assert.strictEqual(typeof(people), typeof([]));
                for (let index = 1; index < people.length; index++) {
                    assert.strictEqual(people[index-1]['name'] <= people[index]['name'], true);
                }
                done();
            });
        });

        it('can sort on height', (done) => {
            logic = new SwapiLogic();
            logic.getPeople('height', (people) => {
                assert.strictEqual(typeof(people), typeof([]));
                for (let index = 1; index < people.length; index++) {
                    assert.strictEqual(people[index-1]['height'] <= people[index]['height'], true);
                }
                done();
            });
        });

        it('can sort on mass', (done) => {
            logic = new SwapiLogic();
            logic.getPeople('mass', (people) => {
                assert.strictEqual(typeof(people), typeof([]));
                for (let index = 1; index < people.length; index++) {
                    assert.strictEqual(people[index-1]['mass'] <= people[index]['mass'], true);
                }
                done();
            });
        });
    });

    describe('getPlanets', function () {
        // This test seems to have some intermittent failures. It occasionally doesn't get a resident's name populated, but in practice it doesn't happen on the server running
        // I think it may have something to do with the testing framework itself getting callbacks activated before they are ready. :shrug: Unfortunately, I don't have time
        // right now to dig into it more. But when this fails, it is always on a different person who isn't being populated.
        it('populates resident names', (done) => {
            logic = new SwapiLogic();
            logic.getPlanets((planets) => {
                assert.strictEqual(typeof(planets), typeof([]));
                planets.forEach(planet => {
                    planet['residents'].forEach(residentName => {
                        assert.strictEqual(typeof(residentName), typeof("string"));
                        assert.strictEqual(!residentName.includes('/'), true);
                    });
                });
                done();
            });
        });
    });
});
