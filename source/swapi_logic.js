const SwapiReader = require('./swapi_reader');

/**
 * SwapiLogic uses a singleton pattern so that separate uses of it will use the same underlying readers
 * This will allow taking advantage of the caching for faster results.
 */
class SwapiLogic {
    /**
     * constructor. Initalizes desired data readers
     */
    constructor() {
        if (!SwapiLogic._instance) {
            SwapiLogic._instance = this;

            this.peopleReader = new SwapiReader('people/');
            this.planetReader = new SwapiReader('planets/');
    
            // Private function used for sorting people
            this._compare = function(a, b) {
                if (a > b) return 1;
                if (b > a) return -1;
                return 0;
            };
        }
        else return SwapiLogic._instance;
    };

    /**
     * Another way to get the singleton instace of SwapiLogic other than using the constructor
     * @returns the singleton instance of SwapiLogic
     */
    static getInstance() {
        return this._instance;
    };

    /**
     * Gets the list of people, possibly sorted by some value
     * @param {String | null} sortKey what property the people should be sorted by. No sort if a falsey value or if the key isn't found on the person object
     * @param {*} callback the callback function to be called with the list of people after sorting
     */
    getPeople(sortKey, callback) {
        this.peopleReader.getAll((people) => {
            people = people.slice(); // Make a copy of data, so we don't mutate the order in the reader's cache.
            if (sortKey && people[0].hasOwnProperty(sortKey)) {
                people.sort((a, b) => { return this._compare(a[sortKey], b[sortKey]); });
            }
            callback(people);
        });
    };

    /**
     * Gets the list of planets, with resedent names supplied
     * @param {Function} callback the callback function to be called with the list of planets after processing
     */
    getPlanets(callback) {
        this.planetReader.getAll((planets) => {
            planets = planets.slice(); // Make a copy of planets, so we don't mutate anything in the reader's cache.
            for (let planetIndex = 0; planetIndex < planets.length; planetIndex++) {
                let planet = JSON.parse(JSON.stringify(planets[planetIndex])); // Make a copy of each planet, for a simlar reason.
                planets[planetIndex] = planet;
                let residentNames = []
                for (let residentIndex = 0; residentIndex < planet['residents'].length; residentIndex++) {
                    let urlParts = planet['residents'][residentIndex].split('/');
                    let id = urlParts[urlParts.length-2]; // urlParts will have an empty string at the end because residentUrl has a trailing '/', so we want the second to last element.
                    this.peopleReader.getResource(id, (person) => {
                        residentNames.push(person['name']);

                        if (residentIndex >= planet['residents'].length - 1) {
                            planet['residents'] = residentNames;

                            if (planetIndex >= planets.length - 1) {
                                callback(planets);
                            }
                        }
                    });
                }
            }
        });
    };
};

module.exports = SwapiLogic;