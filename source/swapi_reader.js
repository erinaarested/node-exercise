var https = require('https');

class SwapiReader {
    /**
     * Constructor for SwapiReader
     * @param {String} resource The Swapi resource to read. Defaults to "people/"
     */
    constructor(resource='people/') {
        // Make sure that the resource ends with a '/'
        if (resource[resource.length -1] != '/'){
            resource = `${resource}/`;
        }
        this.resource = `https://swapi.dev/api/${resource}`;
        this.pageCache = {};
        this.resourceCache = {};

        // Private function to do the common https get that methods need.
        this._doGet = function (url, key, cache, callback) {
            if (typeof cache[key] !== typeof undefined) {
                callback(cache[key]);
            }
            else {
                https.get(url, (swapi_res) => {
                    if (swapi_res.statusCode !== 200) {
                        // Got a different response than expected. Maybe resource not found
                        cache[key] = null;
                        callback(cache[key]);
                    }
                    else {
                        let body = '';
                        swapi_res.on('data', (chunk) => {
                            body += chunk;
                        });
                        swapi_res.on('end', () => {
                            let json = JSON.parse(body);
                            cache[key] = json;
                            callback(cache[key]);
                        });
                    }
                });
            }
        };
    };

    /**
     * An accesser/getter for the resource url
     * @returns {String} the resource url that the reader is set up to use.
     */
    getURL() {
        return this.resource;
    };

    /**
     * Gets a single resource by id
     * @param {Number} id The resource id to retrieve. Must be an integer with value 1 or higher.
     * @param {Function} callback A callback function to be called with the swapi resource.
     */
    getResource(id, callback) {
        if (id != parseInt(id, 10)) {
            throw new Error(`Can't get resource with id ${id}`);
        }
        if (id < 1) {
            throw new Error(`Can't get resource id ${id} (less than 1)`);
        }

        let url = `${this.resource}${id}/`;
        this._doGet(url, id, this.resourceCache, callback);
    };

    /**
     * Gets the resources from Swapi at the given page number
     * @param {Number} pageNumber The page number to get. Must be an integer, with value 1 or greater.
     * @param {Function} callback A callback function to be called with the swapi page of resources. Example: { count: 82, next:'http://swapi.dev/api/people/?page=3', previous: 'http://swapi.dev/api/people/?page=1', results: [{name:'Luke Skywalker',...},...] }
     */
    getPage(pageNumber, callback) {
        if (pageNumber != parseInt(pageNumber, 10)) {
            error = new Error(`Can't get resource page with pageNumber ${pageNumber}`);
            throw error;
        }
        if (pageNumber < 1) {
            error = new Error(`Can't get resource at a page number ${pageNumber} (less than 1)`);
            throw error;
        }

        let url = `${this.resource}?page=${pageNumber}`;
        this._doGet(url, pageNumber, this.pageCache, callback);
    };

    /**
     * Gets all resources from all pages on swapi
     * @param {Function} callback a callback function to be called with the aggregated resources across all pages.
     */
    getAll(callback) {
        if (typeof this.pageCache['all'] !== typeof undefined) {
            callback(this.pageCache['all']);
        }
        else {
            let result = [];
            let page = 0;
            let next = null;

            // Use a recursive function that calls itself in the callback of getPage.
            // We can use the fact that swapi provides a 'next' field in its json. it is a url if a next page exists, or is null if there is no more.
            let appendNextPage = () => {
                this.getPage(++page, (pageData) => {
                    result = result.concat(pageData['results']);
                    if (!!pageData['next']) {
                        appendNextPage();
                    }
                    else {
                        this.pageCache['all'] = result;
                        callback(this.pageCache['all']);
                    }
                });
            };

            appendNextPage();
        }
    }
};

module.exports = SwapiReader;