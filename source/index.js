const express = require('express');
const SwapiLogic = require('./swapi_logic');
const swapiLogic = new SwapiLogic();
const app = express();
const port = 3000;


app.get('/', (req, res) => {
    res.send("Let's do this!");
});

app.get('/people', (req, res) => {
    let validSortKeys = ['name', 'mass', 'height'];
    let sortKey = req.query.sortBy;
    if (!validSortKeys.includes(sortKey)) {
        sortKey = null;
    }
    swapiLogic.getPeople(sortKey, (people) => {
        res.send(people);
    })
});

app.get('/planets', (req, res) => {
    swapiLogic.getPlanets((planets) => {
        res.send(planets);
    })
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});